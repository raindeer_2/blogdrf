from rest_framework import generics, permissions
from django.contrib.auth import get_user_model

from rest_framework import viewsets

from posts.models import Post
from .serializers import PostSerializer, UserSerializer




class PostViewSet(viewsets.ModelViewSet):
    #permission_classes = (permissions.IsAuthenticated,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

